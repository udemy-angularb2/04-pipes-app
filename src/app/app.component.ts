import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  name: string = 'antonio hernandez';
  value: number = 1000;

  obj = {
    name: 'Lizbeth Villeda'
  }


  showName() {
    console.log( this.name );
  }

}
