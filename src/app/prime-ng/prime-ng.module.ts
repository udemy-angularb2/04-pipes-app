import { NgModule } from '@angular/core';

import { ButtonModule } from 'primeng-lts/button';
import { CardModule } from 'primeng-lts/card';
import { MenubarModule } from 'primeng-lts/menubar';

@NgModule({
  declarations: [],
  exports: [
    ButtonModule,
    CardModule,
    MenubarModule
  ]
})
export class PrimeNgModule { }
