import { Component } from '@angular/core';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styles: [
  ]
})
export class BasicosComponent {

  namelower: string = 'antonio';
  nameUpper: string = 'ANTONIO';
  nameComplete: string = 'AntoNio hernANdez';

  customDate: Date = new Date();

  constructor() { }



}
